package net.montessquio.utils;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ContainerWorkbench;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Item;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.world.IInteractionObject;
import net.minecraft.world.World;

public class PortableCraftingTable extends Item {
	public PortableCraftingTable(Properties b) {
		super(b);
	}
	
	public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
	    if (!world.isRemote) {
	        player.displayGui(new IInteractionObject() {
	            @Override
	            public boolean hasCustomName() {
	                return false;
	            }

	            @Override
	            public ITextComponent getName() {
	                return new TextComponentTranslation(Blocks.CRAFTING_TABLE.getTranslationKey() + ".name");
	            }

	            @Override
	            public ITextComponent getCustomName() {
	                    return null;
	            }

	            @Override
	            public String getGuiID() {
	                return "minecraft:crafting_table";
	            }

	            @Override
	            public Container createContainer(InventoryPlayer playerInventory, EntityPlayer playerIn) {
	                return new ContainerWorkbench(player.inventory, world, new BlockPos(player.posX, player.posY, player.posZ)) {
	                    @Override
	                    public boolean canInteractWith(EntityPlayer playerIn) {
	                        return true;
	                    }
	                };
	            }
	        });
	    }
	    return new ActionResult<>(EnumActionResult.PASS, player.getHeldItem(hand));
	}
}
