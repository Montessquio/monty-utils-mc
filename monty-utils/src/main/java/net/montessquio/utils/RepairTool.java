package net.montessquio.utils;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;

public class RepairTool extends Item {
	public RepairTool(Properties b) {
		super(b);
	}
	
	@Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, EnumHand hand) {
		EnumHand otherHand; if(hand == EnumHand.MAIN_HAND) {otherHand = EnumHand.OFF_HAND;} else { otherHand = EnumHand.MAIN_HAND; }
		ItemStack thisItem = player.getHeldItem(hand);
		ItemStack it = player.getHeldItem(otherHand);
		
		// Ensure that we're not repairing another repair tool.
		// That would break the game.
		// UTIL_ITEMS position 0 is repair tool.
		if(it.getItem() != Utils.UTIL_ITEMS[0].b()) {
			// Only repair if the item is damaged.
	        if(it.isDamaged() && !world.isRemote()) { 
	        	it.setDamage(0);
	        	thisItem.damageItem(1, player);
	        }
		}
        return new ActionResult<ItemStack>(EnumActionResult.SUCCESS, thisItem);
	}	
}
