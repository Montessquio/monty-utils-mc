package net.montessquio.utils.helpers;

// Represents an immutable two-item tuple type.
public class Tuple<T1, T2> {
	private T1 item1;
	private T2 item2;
	
	public Tuple(T1 a, T2 b) {
		item1 = a;
		item2 = b;
	}
	
	public T1 a() { return item1; }
	public T2 b() { return item2; }
}
