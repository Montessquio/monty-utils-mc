package net.montessquio.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.dimdev.rift.listener.ItemAdder;
import org.dimdev.rift.listener.MinecraftStartListener;

import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.launchwrapper.LogWrapper;
import net.minecraft.util.ResourceLocation;

import net.montessquio.utils.helpers.Tuple;

public class Utils implements MinecraftStartListener, ItemAdder {
	public static final Logger LOGGER = LogManager.getLogger();
	
	@Override
	public void onMinecraftStart() {
		LOGGER.info("DoubleDurability has been loaded into Minecraft.");
	}

	@SuppressWarnings("unchecked")
	// A list of every item to register. This is iterated down in registerItems()
	// The order should NOT be changed as index operations are hardcoded!
	public static Tuple<ResourceLocation, Item>[] UTIL_ITEMS = (Tuple<ResourceLocation, Item>[]) new Tuple[] {
			new Tuple<ResourceLocation, Item>(
				new ResourceLocation(
					"montyutils", "repair_tool"
				),
				new RepairTool(
					new Item.Properties()
					.group(ItemGroup.TOOLS)
					.maxStackSize(1)
					.defaultMaxDamage(64)
				)
			),
			new Tuple<ResourceLocation, Item>(
				new ResourceLocation(
					"montyutils", "portable_crafting_table"
				),
				new PortableCraftingTable(
						new Item.Properties()
						.group(ItemGroup.TOOLS)
						.maxStackSize(1)
				)
			)
	};
	
    @Override
    public void registerItems() {
    	for(int i = 0; i < UTIL_ITEMS.length; i++) {
    		Item.register(UTIL_ITEMS[i].a(), UTIL_ITEMS[i].b());
    	}
    	LogWrapper.info("Registered " + UTIL_ITEMS.length + " Items");
    }
}
